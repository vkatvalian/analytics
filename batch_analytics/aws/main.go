package aws

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	tokenURL        = "http://169.254.169.254/latest/api/token"
	metadataURL     = "http://169.254.169.254/latest/meta-data/"
	tokenTTLSeconds = "21600"
)

func GetMetadata(path string) (value string, err error) {
	token, err := getIMDSV2Token()
	if err != nil {
		fmt.Println(err)
	}

	req, err := http.NewRequest("GET", metadataURL+path, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Set("X-Aws-Ec2-Metadata-Token", token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	value = string(body)

	fmt.Println(value)

	return value, err
}

func getIMDSV2Token() (token string, err error) {
	req, err := http.NewRequest("PUT", tokenURL, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Set("X-Aws-Ec2-Metadata-Token-Ttl-Seconds", tokenTTLSeconds)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	token = string(body)

	return token, err
}
