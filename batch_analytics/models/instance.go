package models

import (
	"github.com/uptrace/bun"
	"time"
)

type Instances struct {
	bun.BaseModel `bun:"table:instances"`

	ID            int64       `bun:"id,pk,autoincrement"`
	InstanceID    string      `bun:"instance_id"`
	MetricName    string      `bun:"metic_name"`
	DimensionName string      `bun:"dimension_name"`
	Values        []float64   `bun:"values"`
	Timestamps    []time.Time `bun:"timestamps"`
}
