package database

import (
	"database/sql"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/mysqldialect"
	// "context"
	// "gitlab.com/vkatvalian/batchanalytics/models"
	_ "github.com/go-sql-driver/mysql"
)

var db *bun.DB

func Connect(dsn string) {
	sqldb, err := sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}

	db = bun.NewDB(sqldb, mysqldialect.New())
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	/*
		    _, err = db.NewCreateTable().
			Model((*models.Instances)(nil)).
		//	ForeignKey(`("author_id") REFERENCES "users" ("id") ON DELETE CASCADE`).
			Exec(context.TODO())
			if err != nil {
				panic(err)
			}
	*/
}
