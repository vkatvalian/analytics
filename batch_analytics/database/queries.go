package database

import (
	"context"
	"fmt"
	"gitlab.com/vkatvalian/batchanalytics/models"
	"time"
)

func Insert(instance_id, metric_name, dimension_value string, values []float64, timestamps []time.Time) {
	//	type Instances struct {
	//	bun.BaseModel `bun:"table:instances"`
	//	ID int64  `bun:"id,pk,autoincrement"`
	//	InstanceID string `bun:"instance_id"`
	//	MetricName string `bun:"metic_name"`
	//	DimensionName string `bun:"dimension_name"`
	//	Values	[]string	`bun:"values"`
	//	Timestamps []string `bun:"timestamps"`
	//}

	book := &models.Instances{InstanceID: instance_id, MetricName: metric_name, DimensionName: dimension_value, Values: values, Timestamps: timestamps}
	res, err := db.NewInsert().Model(book).Exec(context.TODO())
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(res)
}
