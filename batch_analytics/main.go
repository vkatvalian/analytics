package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatch"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatch/types"
	"github.com/aws/aws-sdk-go/aws"
	"os"
	"time"

	"gitlab.com/vkatvalian/batchanalytics/database"
)

type CWListMetricsAPI interface {
	ListMetrics(ctx context.Context,
		params *cloudwatch.ListMetricsInput,
		optFns ...func(*cloudwatch.Options)) (*cloudwatch.ListMetricsOutput, error)
}

type CWGetMetricDataAPI interface {
	GetMetricData(ctx context.Context, params *cloudwatch.GetMetricDataInput, optFns ...func(*cloudwatch.Options)) (*cloudwatch.GetMetricDataOutput, error)
}

func GetMetrics(c context.Context, api CWGetMetricDataAPI, input *cloudwatch.GetMetricDataInput) (*cloudwatch.GetMetricDataOutput, error) {
	return api.GetMetricData(c, input)
}

func GetMetricsList(c context.Context, api CWListMetricsAPI, input *cloudwatch.ListMetricsInput) (*cloudwatch.ListMetricsOutput, error) {
	return api.ListMetrics(c, input)
}

func Get(id, namespace, metricName, dimensionName, dimensionValue, stat string, period, diffInMinutes int) {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		panic("configuration error, " + err.Error())
	}

	client := cloudwatch.NewFromConfig(cfg)

	input := &cloudwatch.GetMetricDataInput{
		EndTime:   aws.Time(time.Unix(time.Now().Unix(), 0)),
		StartTime: aws.Time(time.Unix(time.Now().Add(time.Duration(-diffInMinutes)*time.Minute).Unix(), 0)),
		MetricDataQueries: []types.MetricDataQuery{
			types.MetricDataQuery{
				Id: aws.String(id),
				MetricStat: &types.MetricStat{
					Metric: &types.Metric{
						Namespace:  aws.String(namespace),
						MetricName: aws.String(metricName),
						Dimensions: []types.Dimension{
							types.Dimension{
								Name:  aws.String(dimensionName),
								Value: aws.String(dimensionValue),
							},
						},
					},
					Period: aws.Int32(int32(period)),
					Stat:   aws.String(stat),
				},
			},
		},
	}

	result, err := GetMetrics(context.TODO(), client, input)
	if err != nil {
		fmt.Println(err)
	}

	for _, el := range result.MetricDataResults {
		database.Insert(dimensionValue, metricName, namespace, el.Values, el.Timestamps)
	}

	// fmt.Println("Metric Data:", result.MetricDataResults[0].Values, *result.MetricDataResults[0].Label, result.MetricDataResults[0].Messages)
}

func main() {
	// dsn := fmt.Sprintf(dsn_)
	database.Connect(os.Getenv("DSN"))

	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		panic("configuration error, " + err.Error())
	}

	client := cloudwatch.NewFromConfig(cfg)

	input := &cloudwatch.ListMetricsInput{}

	result, err := GetMetricsList(context.TODO(), client, input)
	if err != nil {
		fmt.Println("Could not get metrics")
	}

	id := "e1"
	period := 30
	diffInMinutes := 10
	stat := "Sum"

	for _, m := range result.Metrics {
		for _, d := range m.Dimensions {
			Get(id, *m.Namespace, *m.MetricName, *d.Name, *d.Value, stat, period, diffInMinutes)
		}
	}
}
