package models

import (
	"github.com/uptrace/bun"
	"time"
)

type Instances struct {
	bun.BaseModel `bun:"table:instances"`

	InstanceID string `json:"instance_id"`
	//	MetricName string `bun:"metic_name"`
	//	DimensionName string `bun:"dimension_name"`
	Values     []float64   `json:"values"`
	Timestamps []time.Time `json:"timestamps"`
}
