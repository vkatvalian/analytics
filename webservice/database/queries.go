package database

import (
	"context"
	"fmt"

	"gitlab.com/vkatvalian/analytics/models"
)

func GetInstance(ctx context.Context, instance_id string) *models.Instances {
	users := &models.Instances{}
	err := db.NewSelect().
		Model(users).
		Where("instance_id = ?", instance_id).
		Scan(ctx, users)

	if err != nil {
		fmt.Println(err)
	}

	return users
}
