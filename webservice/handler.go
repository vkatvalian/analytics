package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/vkatvalian/analytics/database"
	// "gitlab.com/vkatvalian/analytics/models"
	// "github.com/gorilla/mux"
	// "io/ioutil"
	"log"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	log.Println("path:", r.URL.Path)
	http.ServeFile(w, r, "static/"+r.URL.Path)
}

func Instances(w http.ResponseWriter, r *http.Request) {
	// hardcoded part, input the instance_id to get the data
	posts := database.GetInstance(r.Context(), "mysql")

	bs, err := json.Marshal(posts)
	if err != nil {
		ReturnError(w, err)
		return
	}

	fmt.Fprint(w, string(bs))
}

func ReturnError(w http.ResponseWriter, err error) {
	fmt.Println(err)
}
