package main

import (
	"os"
	"github.com/gorilla/mux"
	"net/http"

	"gitlab.com/vkatvalian/analytics/database"
)

func main() {
//	dsn := fmt.Sprintf(dsn_)
	database.Connect(os.Getenv("DSN"))

	r := mux.NewRouter()
	sr := r.PathPrefix("/api").Subrouter()
	sr.HandleFunc("/instances", Instances)
	r.HandleFunc("/{rest:.*}", handler)

	http.Handle("/", r)
	// corsObj := handlers.AllowedOrigins([]string{"*"})
	// log.Fatal(http.ListenAndServe(":3000", handlers.CORS(corsObj)(router)))
	http.ListenAndServe("localhost:3000", nil) // handlers.CORS(corsObj)(r))
}
